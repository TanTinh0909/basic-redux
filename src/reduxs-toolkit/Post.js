const { createSlice } = require("@reduxjs/toolkit");

const postSlice = createSlice({
  name: "post",
  initialState: [],
  reducers: {
    addPost: (state, action) => {
      state.push(action.payload);
    },
  },
});

const { reducer, actions } = postSlice;
export const { addPost } = actions;
export default reducer;
