import React from "react";
import "../css/ListHobby.css";
const ListHobby = (props) => {
  const { hobbylist, activateId, handleHobbyClick } = props;

  const handleClick = (hobby) => {
    if (handleHobbyClick) {
      handleHobbyClick(hobby);
    }
  };
  const hihi = () => {
    console.log(123456789);
  };
  return (
    <div>
      <ul>
        {hobbylist.map((item) => (
          <li
            key={item.id}
            className={item.id === activateId ? "active hihi" : "hihi"}
            onClick={() => (handleHobbyClick ? handleClick(item) : hihi())}
          >
            {item.title}
          </li>
        ))}
      </ul>
    </div>
  );
};
export default ListHobby;
