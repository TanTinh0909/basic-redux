import { configureStore } from "@reduxjs/toolkit";
import postReducer from "reduxs-toolkit/Post";

// const store = createStore(rootReducer);
const rootReducer = {
  posts: postReducer,
};
const store = configureStore({
  reducer: rootReducer,
});
export default store;
