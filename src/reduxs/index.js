import { combineReducers } from "redux";
import hobbyRedux from "./hobby";
import userRedux from "./user";
const rootReducer = combineReducers({
  hobby: hobbyRedux,
  user: userRedux,
});

export default rootReducer;
