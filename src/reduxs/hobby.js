const initialState = {
  list: [],
  activate: null,
};

const hobbyRedux = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_HOBBY":
      const newList = [...state.list];
      newList.push(action.payload);
      return {
        ...state,
        list: newList,
      };
    case "SET_ACTIVE_HOBBY":
      const newActivate = action.payload.id;
      return { ...state, activate: newActivate };

    default:
      return state;
  }
};
export default hobbyRedux;
