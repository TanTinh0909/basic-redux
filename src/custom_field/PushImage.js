import React from "react";
import { FormGroup, Input } from "reactstrap";
import PropTypes from "prop-types";

PushImage.propTypes = {
  name: PropTypes.string,
  imageUrl: PropTypes.string,
  onImageUrlChange: PropTypes.func,
  onPushImageUrl: PropTypes.func,
};
PushImage.defaultProps = {
  name: "",
  imageUrl: "",
  onImageUrlChange: null,
  onPushImageUrl: null,
};

function PushImage(props) {
  const { name, imageUrl, onImageUrlChange, onPushImageUrl } = props;

  const uploadingImage = (e) => {
    const data = new FormData();
    data.append("file", e.currentTarget.files[0]);
    data.append("upload_preset", "twitter");
    data.append("cloud_name", "dqsjs4uyz");
    fetch("https://api.cloudinary.com/v1_1/dqsjs4uyz/image/upload", {
      method: "post",
      body: data,
    })
      .then((res) => res.json())
      .then((data) => {
        onImageUrlChange(data.url);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <FormGroup>
      <Input
        type="file"
        name={name}
        onBlur={onPushImageUrl}
        onChange={(e) => uploadingImage(e)}
      />
      <img src={imageUrl} alt="sdfgh" width="10%" />
    </FormGroup>
  );
}
export default PushImage;
