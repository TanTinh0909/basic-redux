import React from "react";
import { FormFeedback, FormGroup, Input, Label } from "reactstrap";
import PropTypes from "prop-types";
import { ErrorMessage } from "formik";
RadioField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  options: PropTypes.array,
};
RadioField.defaultProps = {
  type: "radio",
  label: "",
  disabled: false,
  options: [],
};

function RadioField(props) {
  const { field, options, form, type, label, disabled } = props;
  const { name } = field;
  return (
    <FormGroup className="label_checkbox">
      {label}
      {options.map((item) => (
        <Label key={item.id} htmlFor={item.id}>
          {item.name}
          <Input
            id={item.id}
            {...field}
            value={item.id}
            type={type}
            name={name}
            disabled={disabled}
          />
        </Label>
      ))}
    </FormGroup>
  );
}
export default RadioField;
