import React from "react";
import { Checkbox } from "antd";

const InputFeedback = ({ error }) =>
  error ? <div className="input-feedback">{error}</div> : null;

function CheckboxGroup(props) {
  const { field, options, form, label } = props;
  const { name, value } = field;
  const { setFieldValue, errors } = form;

  console.log(errors[name]);
  const handleChange = (checkedValues) => {
    setFieldValue(name, checkedValues);
  };

  return (
    <div className="form-group">
      <label>{label}</label> <br />
      <br />
      <Checkbox.Group
        options={options}
        defaultValue={value}
        onChange={handleChange}
      />
      <InputFeedback error={errors[name]} />
    </div>
  );
}

export default CheckboxGroup;
