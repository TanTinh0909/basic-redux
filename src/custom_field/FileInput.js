import React from "react";
import { FormFeedback, FormGroup, Label } from "reactstrap";
import PropTypes from "prop-types";
import PushImage from "./PushImage";
import { ErrorMessage } from "formik";
FileInput.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  label: PropTypes.string,
};
FileInput.defaultProps = {
  label: "",
};

function FileInput(props) {
  const { field, form, label } = props;
  const { name, value, onBlur } = field;
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];
  const handleImageChange = (newImageUrl) => {
    form.setFieldValue(name, newImageUrl);
  };

  return (
    <FormGroup>
      {label && <Label for={name}>{label}</Label>}
      <PushImage
        name={name}
        imageUrl={value}
        onImageUrlChange={handleImageChange}
        onPushImageUrl={onBlur}
      />
      <div className={showError ? "is-invalid" : ""}></div>
      <ErrorMessage name={name} component={FormFeedback} />
    </FormGroup>
  );
}
export default FileInput;
