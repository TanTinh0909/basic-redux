import { FastField, Form, Formik } from "formik";
import React from "react";
import PropTypes from "prop-types";
import InputField from "custom_field/InputField";
import SelectField from "custom_field/SelectField";
import CheckboxField from "custom_field/CheckboxField";
import RadioField from "custom_field/RadioField";
import FileInput from "custom_field/FileInput";
import { Button } from "reactstrap";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { addPost } from "reduxs-toolkit/Post";
FromFormik.propTypes = { onSubmit: PropTypes.func };
FromFormik.defaultProps = { onSubmit: null };

function FromFormik(props) {
  const initialValues = {
    title: "",
    category: null,
    checked: [],
    radio: null,
    file: "",
  };
  const validationSchema = Yup.object().shape({});

  const SELECT_OPTIONS = [
    { value: 1, label: "Technology" },
    { value: 2, label: "Education" },
    { value: 3, label: "Nature" },
    { value: 4, label: "Animals" },
    { value: 5, label: "Styles" },
  ];

  const CHECKBOX_OPTIONS = [
    { id: 11, name: "Technology" },
    { id: 22, name: "Education" },
    { id: 33, name: "Nature" },
    { id: 44, name: "Animals" },
    { id: 55, name: "Styles" },
  ];

  const RADIO_OPTIONS = [
    { id: 111, name: "Tinh" },
    { id: 222, name: "Thien" },
    { id: 333, name: "Hung" },
    { id: 444, name: "Nam" },
    { id: 555, name: "Hai" },
  ];

  const dispatch = useDispatch();
  const history = useHistory();
  const handlerSubmit = (value) => {
    const action = addPost(value);
    console.log({ action });
    dispatch(action);
  };

  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => handlerSubmit(values)}
      >
        {(formikProps) => {
          // const { values, errors, touched } = formikProps;
          // console.log(values, errors, touched);
          return (
            <Form>
              <FastField
                name="title"
                component={InputField}
                label="Title"
                placeholder="Eg: input field"
              />
              <FastField
                name="category"
                component={SelectField}
                label="Category"
                placeholder="Eg: selected field"
                options={SELECT_OPTIONS}
              />
              <FastField
                name="checked"
                component={CheckboxField}
                label="Checked"
                options={CHECKBOX_OPTIONS}
              />
              <FastField
                name="radio"
                component={RadioField}
                label="Radio"
                options={RADIO_OPTIONS}
              />
              <FastField name="file" component={FileInput} label="File" />
              <Button type="submit">submit</Button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default FromFormik;
