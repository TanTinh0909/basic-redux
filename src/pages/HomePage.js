import { addNewHobby, setActiveHobby } from "actions/hobby";
import ListHobby from "components/ListHobby";
import React from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import FromFormik from "./FromFormik";

const HomePage = () => {
  // const hobbylist = useSelector((state) => state.hobby.list);
  // const activateId = useSelector((state) => state.hobby.activate);
  // const dispatch = useDispatch();

  // const handleAllhobby = () => {
  //   const newHobby = {
  //     id: Math.floor(Math.random() * 100000),
  //     title: `hihi ${Math.floor(Math.random() * 100000)}`,
  //   };
  //   const action = addNewHobby(newHobby);
  //   dispatch(action);
  // };
  // const handleHobbyClick = (hobby) => {
  //   const action = setActiveHobby(hobby);
  //   dispatch(action);
  // };
  return (
    <div>
      {/** <button onClick={handleAllhobby}>add hobby</button>

      <ListHobby
        hobbylist={hobbylist}
        activateId={activateId}
        handleHobbyClick={handleHobbyClick}
      />**/}
      <FromFormik />
    </div>
  );
};
export default HomePage;
